#!/usr/bin/python

import pyperclip

def main():
	pesan = "Indonesia tanah airku. Tanah tumpah darahku. Disanalah aku berdiri jadi pandu ibuku."

	enResult = caesar("encrypt", pesan)
	deResult = caesar("decrypt", enResult)

	print("encypted:")
	print(enResult)
	print("")
	print("decrypted:")
	print(deResult)

def caesar(mode, pesan):
	HURUF = "HIJKLMN4BCD3FG0PQR5TU"
	kunci = 9
	ubah = ""
	pesan = pesan.upper()

	i = len(pesan) - 1

	for symbol in pesan:
		if symbol in HURUF:
			nomor = HURUF.find(symbol)
			if mode == "encrypt":
				nomor = nomor + kunci
			elif mode == "decrypt":
				nomor = nomor - kunci

			if nomor >= len(HURUF):
				nomor = nomor - len(HURUF)
			elif nomor < 0:
				nomor = nomor + len(HURUF)

			ubah = ubah + HURUF[nomor]
		else:
			ubah = ubah + symbol

	return ubah

if __name__ == '__main__':
	main()

# pyperclip.copy(ubah)