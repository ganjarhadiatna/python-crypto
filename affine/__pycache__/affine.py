#!/usr/bin/python3

import sys, pyperclip, kriptomath, random

SYMBOLS = """ !"#$%&'()*+,-
./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmno
pqrstuvwxyz{|}~"""

def main():
	pesanSaya = """"A computer would deserve to be called intteligent
	if it could deceive a human into believing that it was a human."
	-Alan Turing
	"""

	kunciSaya = 2023

	modeSaya = "enkripsi"

	if modeSaya == "enkripsi":
		ubah = enkripsiPesan(kunciSaya, pesanSaya)
	elif modeSaya == "dekripsi":
		ubah = dekripsiPesan(kunciSaya, pesanSaya)

	print("Kunci: %s" % (kunciSaya))
	print("%si: " % (modeSaya.tile()))
	print(ubah)

	# penggunaan pyperclip


def cariBagianKunci(kunci):
	kunciA = kunci // len(SYMBOLS)
	kunciB = kunci % len(SYMBOLS)

	return (kunciA, kunciB)

def cekKunci(kunciA, kunciB, mode):

	if kunciA == 1 and mode == "enkripsi":
		sys.exit("Sandi Affine akan menjadi lebih lemah \
		ketika A ditentukan menjadi 1. silahkan pilih \
		kunci yang berbeda.")

	if kunciB == 0 and mode == "enkripsi":
		sys.exit("Sandi Affine akan menjadi lebih lemah \
		ketika B ditentukan menjadi 0. silahkan pilih \
		kunci yang berbeda.")

	if kunciA < 0 or kunciB < 0 or kunciB > len(SYMBOLS) -1:
		sys.exit("Kunci A (%s) dan simbol ukuran yang \
		ditentukan (%s) secara relatif bukanlah yang \
		paling utama. Silahkan pilih kunci yang berbeda.\
		" % (kunciA, len(SYMBOLS)))
		


def enkripsiPesan(kunci, pesan):
	
	kunciA, kunciB = cariBagianKunci(kunci)
	cekKunci(kunciA, kunciB, "enkripsi")
	
	sandiTeks = ""

	for symbol in pesan:
		if symbol in SYMBOLS:
			symIndex = SYMBOLS.find(symbol)
			sandiTeks += SYMBOLS[(symIndex * kunciA + kunciB) % len(SYMBOLS)]
		else:
			sandiTeks += symbol

	return sandiTeks


def dekripsiPesan(kunci, pesan):
	
	kunciA, kunciB = cariBagianKunci(kunci)
	cekKunci(kunciA, kunciB, "dekripsi")

	teksAwal = ""

	kunciUntukMembalikanA = kriptomath.cariModMembalikan(kunciA, len(SYMBOLS))

	for symbol in pesan:
		if symbol in SYMBOLS:
			symIndex = SYMBOLS.find(symbol)
			teksAwal += SYMBOLS[(symIndex - kunciB) * kunciUntukMembalikanA % len(SYMBOLS)]
		else:
			teksAwal += symbol

		return teksAwal

def menentukanKunciAcak():
	while True:
		kunciA = random.randint(2, len(SYMBOLS))
		kunciB = random.randint(2, len(SYMBOLS))
		if kriptomath.gcd(kunciA, len(SYMBOLS)) == 1:
			return kunciA * len(SYMBOLS) + kunciB

if __name__ == '__main__':
	main()