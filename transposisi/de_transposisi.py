#!/usr/bin/python

import math, pyperclip

def main():
	pesanSaya = "Iaakn nudRa:oahIny ReaATs iAiTr|"
	kunciSaya = 8

	teksawal = deskripsiPesan(kunciSaya, pesanSaya)

	print(teksawal + "|")

def deskripsiPesan(kunci, pesan):
	jumlahKolom = math.ceil(len(pesan) / kunci)

	jumlahBaris = kunci

	jumlahKotakTerisi = (jumlahKolom * jumlahBaris) - len(pesan)

	teksawal = [''] * jumlahKolom

	kolom = 0
	baris = 0

	for symbol in pesan:
		teksawal[kolom] += symbol
		kolom += 1
		if (kolom == jumlahKolom) or (kolom == jumlahKolom - 1 and baris >= jumlahBaris - jumlahKotakTerisi):
			kolom = 0
			baris += 1

	return "".join(teksawal)
	

if __name__ == '__main__':
	main()